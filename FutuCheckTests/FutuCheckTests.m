//
//  FutuCheckTests.m
//  FutuCheckTests
//
//  Created by Martin Richter on 13.06.13.
//  Copyright (c) 2013 Futurice. All rights reserved.
//

#import "FutuCheckTests.h"

@implementation FutuCheckTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in FutuCheckTests");
}

@end
