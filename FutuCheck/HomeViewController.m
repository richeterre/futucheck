//
//  HomeViewController.m
//  FutuCheck
//
//  Created by Martin Richter on 13.06.13.
//  Copyright (c) 2013 Futurice. All rights reserved.
//

#import "HomeViewController.h"
#import <AFNetworking/AFJSONRequestOperation.h>

@interface HomeViewController ()

@property (nonatomic, strong) NSArray *sites;

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"SiteCell"];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Load remote data
    NSURL *url = [NSURL URLWithString:@"sites.json" relativeToURL:kBaseUrl];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
        // Handle success
        NSLog(@"Received data: %@", JSON);
        
        if ([JSON isKindOfClass:[NSArray class]])
        {
            self.sites = JSON;
            [self.tableView reloadData];
        }
        else
        {
            [self showErrorMessage:@"The downloaded data was invalid."];
        }
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
        // Handle failure
        NSLog(@"Error receiving data: %@", error.userInfo);
        [self showErrorMessage:@"No data could be downloaded."];
        
    }];
    
    [operation start];
}

- (void)showErrorMessage:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
    [alert show];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sites.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SiteCell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.sites[indexPath.row];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
