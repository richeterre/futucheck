//
//  main.m
//  FutuCheck
//
//  Created by Martin Richter on 13.06.13.
//  Copyright (c) 2013 Futurice. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
