//
//  AppDelegate.h
//  FutuCheck
//
//  Created by Martin Richter on 13.06.13.
//  Copyright (c) 2013 Futurice. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
